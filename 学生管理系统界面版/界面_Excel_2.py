from tkinter import *
import tkinter.font as tkFont
from PIL import Image,ImageTk
from tkinter import ttk
import tkinter as tk
from tkinter import messagebox
import pandas as pd
import numpy as np


LARGE_FONT= ("Verdana", 20)  #字体为宋体，20号


class Base_Page():
    def __init__(self,master):
        self.root=master
        self.root.config()
        self.root.title("Base page")
        self.root.geometry('200x200')
        Login_Page(self.root)


class Login_Page():
    def __init__(self,master):
        self.tk=tk
        self.master=master
        self.master.geometry('300x600')
        self.Login_Page=tk.Frame(self.master)
        self.Login_Page.pack()
        self.label = Label(self.Login_Page, text="管理系统用户登入", font=LARGE_FONT, bg='black', fg='SkyBlue')
        self.label.pack(pady=80)
        ft2 = tkFont.Font(size=15)

        global var_user_name
        var_user_name=tk.StringVar()
        global var_user_pwd
        var_user_pwd = tk.StringVar()
        Label(self.Login_Page, text='用户名', font=ft2).pack()
        Entry(self.Login_Page, textvariable=var_user_name).pack()
        Label(self.Login_Page, text='密码',font=ft2).pack()
        Entry(self.Login_Page, textvariable=var_user_pwd, show='*').pack()
        Button(self.Login_Page, text='登入', command=self.login_check).pack()
        Button(self.Login_Page, text='没有账户，立即注册',command=self.go_logon_page).pack()

    def login_check(self):
        user_name = var_user_name.get()
        user_pwd = var_user_pwd.get()
        if user_name=="" or user_pwd=="":
            self.tk.messagebox.showwarning(title="警告", message="用户名或密码不能为空！")
        login_data = pd.read_excel('登入信息.xlsx')
        check_name=login_data['用户名']  #用户名：zeng
        check_pwd=login_data['密码']     #密码：666
        flag1 = 0
        flag2 = 0
        for i in check_name:
            if user_name==i:
                flag2=1
                for j in check_pwd:
                    if user_pwd==str(j):
                        flag1=1
                        break

        if (flag1==0 and flag2==0) or (flag2==1 and flag1==0):
            self.tk.messagebox.showwarning(title="提示", message="输入的用户名或密码有误！")
        else:
            self.Login_Page.destroy()
            Main_Page(self.master)

    def go_logon_page(self):
        self.Login_Page.destroy()
        Logon_Page(self.master)



class Logon_Page():
    def __init__(self,master):
        self.tk = tk
        self.master = master
        self.master.geometry('300x600')
        self.Logon_Page = tk.Frame(self.master)
        self.Logon_Page.pack()
        self.label = Label(self.Logon_Page, text="管理系统新用户注册", font=LARGE_FONT, bg='black', fg='SkyBlue')
        self.label.pack(pady=80)
        ft2 = tkFont.Font(size=15)
        global new_user_name
        global new_user_pwd
        global confirm_pwd
        new_user_name=tk.StringVar()
        new_user_pwd=tk.StringVar()
        confirm_pwd=tk.StringVar()
        Label(self.Logon_Page, text='登入用户名', font=ft2).pack()
        Entry(self.Logon_Page, textvariable=new_user_name).pack()
        Label(self.Logon_Page, text='登入密码', font=ft2).pack()
        Entry(self.Logon_Page, textvariable=new_user_pwd, show='*').pack()
        Label(self.Logon_Page, text='确认密码', font=ft2).pack()
        Entry(self.Logon_Page, textvariable=confirm_pwd, show='*').pack()
        Button(self.Logon_Page, text='注册', command=self.sign_on).pack()
        Button(self.Logon_Page, text='登入', command=self.go_login_page).pack()

    def sign_on(self):
        if new_user_name.get()=="" or new_user_pwd.get()=="" or confirm_pwd.get()=="":
            self.tk.messagebox.showwarning(title="警告", message="登入用户名、登入密码或确认密码不能为空！")
        else:
            if new_user_pwd.get() != "" and new_user_pwd.get() != confirm_pwd.get():
                self.tk.messagebox.showwarning(title="警告", message="创建的密码不唯一！")
            else:
                new_ua=new_user_name.get()
                try :
                    new_up=int(new_user_pwd.get())
                except:
                    new_up=new_user_pwd
                logon_data = pd.read_excel('登入信息.xlsx')
                logon_data.loc[len(logon_data)] = [new_ua,new_up]
                pd.DataFrame(logon_data, columns=['用户名','密码']).to_excel('登入信息.xlsx', index=False,header=True)
                self.tk.messagebox.showwarning(title="提示", message="注册成功！")

    def go_login_page(self):
        self.Logon_Page.destroy()
        Login_Page(self.master)

class Main_Page():
    def __init__(self,master):
        self.master=master
        self.master.geometry('300x600')
        self.Main_Page=tk.Frame(self.master)
        self.Main_Page.pack()
        self.label=Label(self.Main_Page,text="学生信息管理系统", font=LARGE_FONT,bg='black',fg='SkyBlue')
        self.label.pack(pady=80)
        ft2 = tkFont.Font(size=20)  # 指定字体大小

        Button(self.Main_Page, text="添加学生信息", font=ft2, width=50, height=2,
               fg='white', bg='gray', activebackground='black',command=self.change,activeforeground='white').pack()
        Button(self.Main_Page, text="删除学生信息", font=ft2, width=50, height=2,command=self.go_delete_page).pack()
        Button(self.Main_Page, text="修改学生信息", font=ft2, width=50, height=2, fg='white',
               bg='gray', activebackground='black',command=self.go_modifypage, activeforeground='white').pack()
        Button(self.Main_Page, text="查询学生信息", font=ft2, width=50, height=2,command=self.go_query).pack()
        Button(self.Main_Page, text='退出系统', height=2, font=ft2, width=50, fg='white', bg='gray',
               activebackground='black', command=self.master.destroy, activeforeground='white').pack()
    def change(self):
        self.Main_Page.destroy()
        Add_Stu(self.master)

    def go_query(self):
        self.Main_Page.destroy()
        Query_Stu(self.master)


    def go_delete_page(self):
        self.Main_Page.destroy()
        Delete_Stu(self.master)

    def go_modifypage(self):
        self.Main_Page.destroy()
        Modify_Stu(self.master)


class Add_Stu():
    def __init__(self,master):
        self.tk=tk
        self.master=master
        self.master.geometry('300x600')
        self.Add_Stu = tk.Frame(self.master)
        self.Add_Stu.pack()
        label = Label(self.Add_Stu, text="添加学生信息", font=LARGE_FONT)
        label.pack(pady=100)
        ft3 = tkFont.Font(size=14)
        ft4 = tkFont.Font(size=12)
        Label(self.Add_Stu, text='学生学号：', font=ft3).pack(side=TOP)
        global e1
        e1 = StringVar()
        # textvariable:文本框的值，是一个StringVar()对象
        Entry(self.Add_Stu,width=30, textvariable=e1, font=ft3, bg='Ivory').pack(side=TOP)
        Label(self.Add_Stu,text='学生姓名：', font=ft3).pack(side=TOP)
        global e2
        e2 = StringVar()
        Entry(self.Add_Stu,width=30, textvariable=e2, font=ft3, bg='Ivory').pack(side=TOP)
        Label(self.Add_Stu,text='学生成绩：', font=ft3).pack(side=TOP)
        global e3
        e3 = StringVar()
        Entry(self.Add_Stu,width=30, textvariable=e3, font=ft3, bg='Ivory').pack(side=TOP)
        Button(self.Add_Stu,text="返回首页", width=8, font=ft4,command=self.back_mainPage).pack(pady=20)
        Button(self.Add_Stu, text="确定保存", width=8, font=ft4, command=self.save).pack()
        Button(self.Add_Stu,text="查询信息", width=8, font=ft4,command=self.go_querypage).pack(pady=20)

    def back_mainPage(self):
        self.Add_Stu.destroy()
        Main_Page(self.master)
    def go_querypage(self):
        self.Add_Stu.destroy()
        Query_Stu(self.master)

    def save(self):
        stu_num=e1.get()
        stu_name=e2.get()
        stu_grade=e3.get()
        if e1.get()=="" or e2.get()=="" or e3.get()=="":
            self.tk.messagebox.showwarning(title="警告", message="学生的学号、姓名、成绩不能为空！")
        else:
            stu_data=pd.read_excel('学生信息.xlsx')
            stu_id=stu_data['学号']
            flag=0
            for i in range(len(stu_id)):
                if int(stu_num)==stu_id[i]:
                    flag=1
                    break
            if flag==1:
                self.tk.messagebox.showwarning(title="警告", message="学号已存在，请重新输入！")
            else:
                stu_data.loc[len(stu_data)] = [int(stu_num),stu_name,int(stu_grade)]
                pd.DataFrame(stu_data, columns=['学号', '姓名','成绩']).to_excel('学生信息.xlsx', index=False, header=True)
                self.tk.messagebox.showwarning(title="提示", message="添加成功！")


class Delete_Stu():
    def __init__(self,master):
        self.tk=tk
        self.master=master
        self.master.geometry('300x600')
        self.Delete_Stu = tk.Frame(self.master)
        self.Delete_Stu.pack()
        label = tk.Label(self.Delete_Stu, text="删除学生信息", font=LARGE_FONT)
        label.pack(pady=100)

        ft3 = tkFont.Font(size=14)
        ft4 = tkFont.Font(size=12)
        Label(self.Delete_Stu, text='请输入你要删除的学生学号：', font=ft3).pack(side=TOP)
        global e4
        e4 = StringVar()
        Entry(self.Delete_Stu, width=30, textvariable=e4, font=ft3, bg='Ivory').pack(side=TOP)
        Button(self.Delete_Stu, text="返回首页", width=8, font=ft4, command=self.back_mainpage).pack(pady=20)
        Button(self.Delete_Stu, text="确定删除", width=8, font=ft4, command=self.del1).pack()
        Button(self.Delete_Stu, text="查询信息", width=8, font=ft4, command=self.go_querypage).pack(pady=20)


    def back_mainpage(self):
        self.Delete_Stu.destroy()
        Main_Page(self.master)

    def go_querypage(self):
        self.Delete_Stu.destroy()
        Query_Stu(self.master)

    def del1(self):
        if e4.get()=="":
            self.tk.messagebox.showwarning(title="警告", message="学生的学号不能为空！")
        else:
            del_id = e4.get()
            stu_data=pd.read_excel('学生信息.xlsx')
            stu_id = stu_data['学号']
            new_stu_data=[]
            flag=0
            for i in range(len(stu_id)):
                if stu_id[i] == int(del_id):
                    new_stu_data = stu_data.drop(index=[i])
                    flag=1
                    break
            if flag==0:
                self.tk.messagebox.showwarning(title="警告", message="不存在该学生学号！！")
            else:
                pd.DataFrame(new_stu_data, columns=['学号', '姓名', '成绩']).to_excel('学生信息.xlsx', index=False, header=True)
                self.tk.messagebox.showwarning(title="提示", message="删除成功！")


class Modify_Stu():
    def __init__(self,master):
        self.tk=tk
        self.master=master
        self.master.geometry('300x600')
        self.Modify_Stu=tk.Frame(self.master)
        self.Modify_Stu.pack()
        tk.Label(self.Modify_Stu, text="修改学生信息", font=LARGE_FONT).pack(pady=100)

        ft3 = tkFont.Font(size=14)
        ft4 = tkFont.Font(size=12)
        Label(self.Modify_Stu, text='请输入你要修改的学生学号：', font=ft3).pack(side=TOP)
        self.e5 = StringVar()
        Entry(self.Modify_Stu, width=30, textvariable=self.e5, font=ft3, bg='Ivory').pack(side=TOP)

        Label(self.Modify_Stu, text='修改后的的学生学号：', font=ft3).pack(side=TOP)
        self.e8 = StringVar()
        Entry(self.Modify_Stu, width=30, textvariable=self.e8, font=ft3, bg='Ivory').pack(side=TOP)

        Label(self.Modify_Stu, text='学生姓名：', font=ft3).pack(side=TOP)
        self.e6 = StringVar()
        Entry(self.Modify_Stu, width=30, textvariable=self.e6, font=ft3, bg='Ivory').pack(side=TOP)

        Label(self.Modify_Stu, text='学生成绩：', font=ft3).pack(side=TOP)
        self.e7 = StringVar()
        Entry(self.Modify_Stu, width=30, textvariable=self.e7, font=ft3, bg='Ivory').pack(side=TOP)

        Button(self.Modify_Stu, text="确定修改", width=8, font=ft4, command=self.modify).pack(pady=20)
        Button(self.Modify_Stu, text="查询信息", width=8, font=ft4, command=self.go_querypage).pack()
        Button(self.Modify_Stu, text="返回首页", width=8, font=ft4, command=self.back_mainpage).pack(pady=20)

    def back_mainpage(self):
        self.Modify_Stu.destroy()
        Main_Page(self.master)

    def go_querypage(self):
        self.Modify_Stu.destroy()
        Query_Stu(self.master)


    def modify(self):
        if self.e5.get()=="" or self.e6.get()=="" or self.e7.get()=="" or self.e8.get()=="":
            self.tk.messagebox.showwarning(title="警告",message="学生的学号、姓名或成绩不能为空！")
        else:

            data_temp = pd.read_excel('学生信息.xlsx')
            stu_id = data_temp['学号']
            m_id = int(self.e5.get())
            rem_id=int(self.e8.get())
            name3 = self.e6.get()
            score3 = int(self.e7.get())
            flag=0
            flag1=0
            if rem_id!=m_id:
                for j in range(len(stu_id)):
                    if rem_id==stu_id[j]:
                        flag1=1
                        break
            if flag1==1:
                self.tk.messagebox.showwarning(title="警告", message="修改后的学号已存在，请重新输入！")
            else:
                for i in range(len(stu_id)):
                    if stu_id[i] == m_id:
                        data_temp.loc[i, '学号'] = rem_id
                        data_temp.loc[i, '姓名'] = name3
                        data_temp.loc[i, '成绩'] = score3
                        flag = 1
                        break
                if flag == 1:
                    pd.DataFrame(data_temp, columns=['学号', '姓名', '成绩']).to_excel('学生信息.xlsx', index=False, header=True)
                    self.tk.messagebox.showwarning(title="提示", message="修改成功！")


class Query_Stu():
    def __init__(self,master):
        self.master=master
        self.master.geometry('300x600')
        self.Query_Stu = tk.Frame(self.master)
        self.Query_Stu.pack()
        label = tk.Label(self.Query_Stu, text="查询学生成绩", font=LARGE_FONT)
        label.pack(pady=100)
        tree_data = ttk.Treeview()
        ft4 = tkFont.Font(size=12)
        # 滚动条
        scro = Scrollbar(self.Query_Stu)
        scro.pack(side=RIGHT, fill=Y)  # side指定Scrollbar为居右；fill指定填充满整个剩余区域。
        lista = Listbox(self.Query_Stu, yscrollcommand=scro.set, width=50)  # Listbox:列表框
        stu_data=pd.read_excel('学生信息.xlsx')
        stu_num=stu_data['学号']
        stu_name=stu_data['姓名']
        stu_grade=stu_data['成绩']
        text = ("                  %-16s%-16s%-16s" % ("学号", "姓名", "成绩"))
        for i in range(len(stu_num)):
            text1 = ("                    %-16s%-16s%-16s" % (stu_num[i], stu_name[i], stu_grade[i]))
            lista.insert(END, text1)
        lista.insert(0, text)
        lista.pack()
        Button(self.Query_Stu, text="返回首页", width=8, font=ft4,command=self.back_mainpage).pack(pady=10)
        Button(self.Query_Stu, text="添加信息", width=8, font=ft4, command=self.go_addpage).pack()
        Button(self.Query_Stu, text="删除信息", width=8, font=ft4, command=self.go_delect_page).pack(pady=10)
        Button(self.Query_Stu, text="修改信息", width=8, font=ft4, command=self.go_modifypage).pack()

    def back_mainpage(self):
        self.Query_Stu.destroy()
        Main_Page(self.master)

    def go_addpage(self):
        self.Query_Stu.destroy()
        Add_Stu(self.master)

    def go_delect_page(self):
        self.Query_Stu.destroy()
        Delete_Stu(self.master)

    def go_modifypage(self):
        self.Query_Stu.destroy()
        Modify_Stu(self.master)

if __name__ == '__main__':
    root=tk.Tk()
    Base_Page(root)
    root.mainloop()


